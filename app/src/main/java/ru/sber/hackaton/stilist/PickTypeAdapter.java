package ru.sber.hackaton.stilist;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class PickTypeAdapter extends BaseAdapter {
    @Override
    public int getCount() {
        return PickType.values().length;
    }

    @Override
    public PickType getItem(int position) {
        return PickType.values()[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
        }
        final TextView title =  (TextView)convertView.findViewById(android.R.id.text1);
        title.setText(getItem(position).getName());

        return convertView;
    }
}
