package ru.sber.hackaton.stilist;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

/**
 * Created by Lazarenko Georgy on 06.06.17.
 * Выбор фотографии
 */

public class PhotoActivity extends AppCompatActivity {

    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int REQUEST_PICK_IMAGE = 2;

    private Button mChooseAnotherPhoto;
    private Button mConfirm;
    private ImageView mPhoto;

    String mImagePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_layout);

        mChooseAnotherPhoto = (Button) findViewById(R.id.choose_another_photo);
        mConfirm = (Button) findViewById(R.id.confirm);
        mPhoto = (ImageView) findViewById(R.id.photo);

        mPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPhotoFromGallery();
            }
        });

        mChooseAnotherPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeDecision();
            }
        });

        mConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToNextScreen();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_PICK_IMAGE) {
                Uri pickedImage = data.getData();
                // Let's read picked image path using content resolver
                String[] filePath = { MediaStore.Images.Media.DATA };
                Cursor cursor = getContentResolver().query(pickedImage, filePath, null, null, null);
                cursor.moveToFirst();
                mImagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));
                cursor.close();


                new LoadPhotoAsyncTask(new LoadPhotoAsyncTask.LoadPhotoListener() {
                    @Override
                    public void onPhotoLoaded(Bitmap bitmap) {
                        mPhoto.setImageBitmap(bitmap);
                    }
                }).execute(mImagePath);

            }else if (requestCode == REQUEST_IMAGE_CAPTURE) {
                // todo
                //Bundle extras = data.getExtras();
                //bitmap = (Bitmap) extras.get("data");
            }
        }
        if (mImagePath != null) {
            makeDecision();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPhoto.invalidate();
    }

    private void getPhotoFromGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_PICK_IMAGE);
    }

/*    private void makePhoto() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }*/

    private void makeDecision() {
        mChooseAnotherPhoto.setVisibility(View.VISIBLE);
        mConfirm.setVisibility(View.VISIBLE);
    }

    private void changeDecision() {
        mChooseAnotherPhoto.setVisibility(View.GONE);
        mConfirm.setVisibility(View.GONE);
        getPhotoFromGallery();
    }

    private void goToNextScreen() {
        PickActivity.startActivity(this, mImagePath);
    }
}