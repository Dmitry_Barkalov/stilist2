package ru.sber.hackaton.stilist;

public enum PickType {
    DRESSES("Платье"),
    JACKET("Жакет");

    private final String mName;

    PickType(String name) {
        this.mName = name;
    }

    public String getName() {
        return mName;
    }
}
