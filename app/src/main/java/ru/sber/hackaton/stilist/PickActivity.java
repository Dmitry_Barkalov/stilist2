package ru.sber.hackaton.stilist;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

public class PickActivity extends AppCompatActivity implements LoadPhotoAsyncTask.LoadPhotoListener{
    public static final String PHOTO_PATH_EXTRA = "PHOTO_PATH_EXTRA";

    private ImageView mPhotoView;
    private Spinner mSpinner;
    private Button mPickButton;
    private Button mEstimateButton;

    private String mImagePath;

    public static void startActivity(Activity parent, String imagePath) {
        Intent intent = new Intent(parent, PickActivity.class);
        intent.putExtra(PHOTO_PATH_EXTRA, imagePath);
        parent.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mImagePath = getIntent().getStringExtra(PHOTO_PATH_EXTRA);

        mPhotoView = (ImageView) findViewById(R.id.photo_view);
        mSpinner = (Spinner) findViewById(R.id.pick_spinner);
        mPickButton = (Button) findViewById(R.id.pick_button);
        mEstimateButton = (Button) findViewById(R.id.estimate_button);

        mPickButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final PickType selectedType = (PickType) mSpinner.getSelectedItem();
                final ShopListActivity.ResultCode code;
                switch(selectedType) {
                    case JACKET:
                        code = ShopListActivity.ResultCode.MAN_JACKETS;
                        break;
                    default:
                        code = ShopListActivity.ResultCode.GREEN_DRESSES;
                }
                ShopListActivity.startActivity(PickActivity.this, code);
            }
        });

        mEstimateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RatingActivity.startActivity(PickActivity.this,mImagePath);
            }
        });

        mSpinner.setAdapter(new PickTypeAdapter());

        new LoadPhotoAsyncTask(this).execute(mImagePath);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPhotoLoaded(Bitmap bitmap) {
        mPhotoView.setImageBitmap(bitmap);
    }
}
