package ru.sber.hackaton.stilist;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.RatingBar;

public class RatingActivity extends AppCompatActivity implements LoadPhotoAsyncTask.LoadPhotoListener {
    public static final String PHOTO_PATH_EXTRA = "PHOTO_PATH_EXTRA";

    private ImageView mPhotoView;
    private RatingBar mFriendsBar;
    private RatingBar mGroupsBar;
    private RatingBar mStylistBar;

    private String mImagePath;

    public static void startActivity(Activity parent, String imagePath) {
        Intent intent = new Intent(parent, RatingActivity.class);
        intent.putExtra(PHOTO_PATH_EXTRA, imagePath);
        parent.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mImagePath = getIntent().getStringExtra(PHOTO_PATH_EXTRA);

        mPhotoView = (ImageView) findViewById(R.id.photo_view);
        mFriendsBar = (RatingBar) findViewById(R.id.friends_rating_bar);
        mGroupsBar = (RatingBar) findViewById(R.id.groups_rating_bar);
        mStylistBar = (RatingBar) findViewById(R.id.stylist_rating_bar);

        mFriendsBar.setRating(5);
        mGroupsBar.setRating(2);
        mStylistBar.setRating(3.5f);

        new LoadPhotoAsyncTask(this).execute(mImagePath);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPhotoLoaded(Bitmap bitmap) {
        mPhotoView.setImageBitmap(bitmap);
    }
}
