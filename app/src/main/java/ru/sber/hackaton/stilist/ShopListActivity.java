package ru.sber.hackaton.stilist;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.RawRes;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import ru.sber.hackaton.stilist.model.Good;

public class ShopListActivity extends AppCompatActivity {
    public static final String GOODS_LIST_RESULT_EXTRA = "GOODS_LIST_RESULT_EXTRA";

    public enum ResultCode {GREEN_DRESSES, MAN_JACKETS}

    private List<Good> mGoods;
    private RecyclerView mRecyclerView;
    private FrameLayout mProgressBar;
    private Button mSortPriceBtn;
    private Button mSortDistenceBtn;
    private Boolean mSortByPrice;
    private int mDefaultBtnColor;

    public static void startActivity(Activity parent, ResultCode code) {
        Intent intent = new Intent(parent, ShopListActivity.class);
        intent.putExtra(GOODS_LIST_RESULT_EXTRA, code.ordinal());
        parent.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mProgressBar = (FrameLayout) findViewById(R.id.progress_bar);
        mRecyclerView = (RecyclerView) findViewById(R.id.goods_list);
        mSortPriceBtn = (Button) findViewById(R.id.sort_price_btn);
        mSortDistenceBtn = (Button) findViewById(R.id.sort_distance_btn);
        mDefaultBtnColor = mSortPriceBtn.getTextColors().getDefaultColor();

        View.OnClickListener toggleListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSortByPrice == null || (v == mSortPriceBtn && !mSortByPrice) || (v == mSortDistenceBtn && mSortByPrice)) {
                    mSortByPrice = (v == mSortPriceBtn) ? true : false;
                    mSortPriceBtn.setTextColor(mSortByPrice ? getColorInt(R.color.colorAccent) : mDefaultBtnColor);
                    mSortDistenceBtn.setTextColor(!mSortByPrice ? getColorInt(R.color.colorAccent) : mDefaultBtnColor);
                    sortListBy(mSortByPrice);
                }
            }
        };

        mSortPriceBtn.setOnClickListener(toggleListener);
        mSortDistenceBtn.setOnClickListener(toggleListener);

        ResultCode resultCode = ResultCode.values()[getIntent().getIntExtra(GOODS_LIST_RESULT_EXTRA, ResultCode.GREEN_DRESSES.ordinal())];
        loadGoods(fakeResultLoad(resultCode));

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(new RecyclerView.Adapter() {
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                LayoutInflater inflater = (LayoutInflater) getSystemService
                        (Context.LAYOUT_INFLATER_SERVICE);
                View v = inflater.inflate(R.layout.shop_list_item, parent, false);
                return new GoodViewItem(v);
            }

            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
                ((GoodViewItem) holder).bindGood(mGoods.get(position), getBaseContext());
            }

            @Override
            public int getItemCount() {
                return mGoods.size();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private String fakeResultLoad(ResultCode resultCode) {
        new AsyncTask() {
            @Override
            protected void onPreExecute() {
                showProgress();
            }

            @Override
            protected Object doInBackground(Object[] params) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                hideProgress();
            }
        }.execute();
        try {
            @RawRes
            int rawResId = 0;
            switch (resultCode) {
                case GREEN_DRESSES:
                    rawResId = R.raw.shop_list;
                    break;
                case MAN_JACKETS:
                    rawResId = R.raw.man_jackets;
                    break;
                default:
                    rawResId = R.raw.shop_list;
            }
            String json = IOUtils.toString(getResources().openRawResource(rawResId), "UTF-8");
            return json;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void loadGoods(String json) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            mGoods = Arrays.asList(mapper.readValue(json, Good[].class));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showProgress() {
        mRecyclerView.setVisibility(View.INVISIBLE);
        mProgressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        mRecyclerView.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.INVISIBLE);
    }

    private static class GoodViewItem extends RecyclerView.ViewHolder {
        protected ImageView mGoodImage;
        protected TextView mShopName;
        protected TextView mShopAddress;
        protected TextView mPrice;
        protected TextView mDistance;
        private Bitmap mCurrentImage;

        public GoodViewItem(View itemView) {
            super(itemView);
            mGoodImage = (ImageView) itemView.findViewById(R.id.good_image);
            mShopName = (TextView) itemView.findViewById(R.id.shop_title);
            mShopAddress = (TextView) itemView.findViewById(R.id.shop_address);
            mPrice = (TextView) itemView.findViewById(R.id.good_price);
            mDistance = (TextView) itemView.findViewById(R.id.distance);
        }

        public void bindGood(Good good, Context context) {
            if (mCurrentImage != null) {
                mCurrentImage.recycle();
                mCurrentImage = null;
            }
            mCurrentImage = loadImage(good.getImageResource(), context);
            mGoodImage.setImageBitmap(mCurrentImage);
            mShopName.setText(good.getShopName());
            mShopAddress.setText(good.getShopAddress());


            mPrice.setText(good.getPrice().toString() + " руб.");
            mDistance.setText(good.getDistance().toString() + " км");
        }

        private Bitmap loadImage(String imageName, Context context) {
            int resId = context.getResources().getIdentifier(imageName, "drawable", context.getPackageName());
            return BitmapFactory.decodeResource(context.getResources(), resId);
        }
    }

    private void sortListBy(final boolean isSortByPrice) {
        Good[] goods = (Good[]) mGoods.toArray();
        Arrays.sort(goods, new Comparator<Good>() {
            @Override
            public int compare(Good o1, Good o2) {
                double v1 = isSortByPrice ? o1.getPrice() : o1.getDistance();
                double v2 = isSortByPrice ? o2.getPrice() : o2.getDistance();
                return v1 == v2 ? 0 : (v1 > v2 ? 1 : -1);
            }
        });
        mGoods = Arrays.asList(goods);
        if (mRecyclerView.getAdapter() != null) {
            mRecyclerView.getAdapter().notifyDataSetChanged();
        }
    }

    private
    @ColorInt
    int getColorInt(@ColorRes int colorRes) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return getColor(colorRes);
        } else {
            return getResources().getColor(colorRes);
        }
    }
}
