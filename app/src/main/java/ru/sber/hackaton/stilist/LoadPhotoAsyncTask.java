package ru.sber.hackaton.stilist;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.lang.ref.WeakReference;

public class LoadPhotoAsyncTask extends AsyncTask<String, Void, Bitmap> {
    private final WeakReference<LoadPhotoListener> mListener;

    public LoadPhotoAsyncTask(LoadPhotoListener listener) {
        mListener = new WeakReference<>(listener);
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        String imagePath = params[0];
       /* BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 4;
       */ return BitmapFactory.decodeFile(imagePath);
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        final LoadPhotoListener listener = mListener.get();
        if(listener != null) {
            listener.onPhotoLoaded(bitmap);
        }
    }

    public interface LoadPhotoListener {
        void onPhotoLoaded(Bitmap bitmap);
    }
}
